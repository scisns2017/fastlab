from setuptools import setup, find_packages
import pathlib

from FastLab import (
    __pkgname__ as PKG_NAME,
    __version__ as VERSION,
    __author__ as AUTHOR,
    __author_email__ as AUTHOR_EMAIL,
    __license__ as LICENSE,
    __summary__ as SUMMARY,
    __url__ as URL
)

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / 'README.md').read_text()

setup(
    name=PKG_NAME,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    description=SUMMARY,
    long_description=README,
    long_description_content_type='text/markdown',
    url=URL,
    license=LICENSE,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GPLv3',
        'Programming Language :: Python :: 3.8',
    ],
    packages=find_packages(),
    python_requires='>=3.8',
    install_requires=[
        'pyyaml',
        'numpy',
        'uncertainties',
        'matplotlib',
        'scipy'
    ],
    extras_require={
        'test': ['coverage', 'pytest', 'opencv-python'],
    },
    project_urls={
        'Bug Reports': f'{URL}/issues',
        'Source': URL,
    }
)
