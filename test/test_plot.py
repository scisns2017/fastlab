
from FastLab.utils.uncertainties import unarray, ucurve_fit
from FastLab.plot import residual_plot, errorbars_plot

import numpy as np
from uncertainties import unumpy as unp

# Generic stuff for simulate a fit


def model(x, a, b):
    return np.sqrt(a * x + b)


def umodel(x, a, b):
    return unp.sqrt(a * x + b)


x = np.array([3., 41., 100.])
y = np.array([5., 15., 25.])
ux = x / 100.
uy = y / 100.

X = unarray(x, ux)
Y = unarray(y, uy)

fitted = ucurve_fit(model, X, Y)


def test_residual_plot():
    # Generic
    residual_plot(umodel, fitted, X, Y, use_ux=False)

    # PDF
    residual_plot(umodel, fitted, X, Y,
                  use_ux=False, figfile='test.tmp.pdf',
                  title='\\LaTeX')
    # TKZ: actually it does not work :(
    try:
        residual_plot(umodel, fitted, X, Y, use_ux=False,
                      figfile='test.tmp.tex')
    except NotImplementedError:
        pass


def test_errorbars_plot():
    errorbars_plot(X, Y, xlogscale=True, ylogscale=True,
                   figfile='errorbarstest.tmp.pdf')
